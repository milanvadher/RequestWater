import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomeDataPage } from '../home-data/home-data';
import { IndustryDataPage } from '../industry-data/industry-data';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  homeData() {
    this.navCtrl.push(HomeDataPage);
  }

  industryData() {
    this.navCtrl.push(IndustryDataPage);
  }

}

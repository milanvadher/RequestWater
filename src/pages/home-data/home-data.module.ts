import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomeDataPage } from './home-data';

@NgModule({
  declarations: [
    HomeDataPage,
  ],
  imports: [
    IonicPageModule.forChild(HomeDataPage),
  ],
})
export class HomeDataPageModule {}

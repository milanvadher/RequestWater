import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FirestoreProvider } from "../../providers/firestore/firestore";

@IonicPage()
@Component({
  selector: 'page-home-data',
  templateUrl: 'home-data.html',
})
export class HomeDataPage {

  members: any;
  name: any;
  area: any;
  purpose: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public fp: FirestoreProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomeDataPage');
  }

  submit() {
    this.fp.userData(this.members,this.name,this.area,this.purpose)
  }

}

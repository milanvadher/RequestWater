import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IndustryDataPage } from './industry-data';

@NgModule({
  declarations: [
    IndustryDataPage,
  ],
  imports: [
    IonicPageModule.forChild(IndustryDataPage),
  ],
})
export class IndustryDataPageModule {}

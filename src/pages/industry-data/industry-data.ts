import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FirestoreProvider } from '../../providers/firestore/firestore';


@IonicPage()
@Component({
  selector: 'page-industry-data',
  templateUrl: 'industry-data.html',
})
export class IndustryDataPage {

  imembers: any;
  iname: any;
  itype: any;
  ipurpose: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public fp: FirestoreProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IndustryDataPage');
  }

  submit() {
    this.fp.industryData(this.imembers,this.iname,this.itype,this.ipurpose);
  }

}

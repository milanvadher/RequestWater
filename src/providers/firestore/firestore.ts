import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';

@Injectable()
export class FirestoreProvider {

  constructor(
    public http: HttpClient, 
    public firestore: AngularFirestore
  ) {
    console.log('Hello FirestoreProvider Provider');
  }

  userData(members, name, area, purpose) {
    try {
      const id = this.firestore.createId();
      this.firestore.doc(`homeData/${id}`).set({
        id: id,
        name: name,
        members: members,
        area: area,
        purpose: purpose
      });
    }
    catch (e) {
      console.log("Error: ", e);
    }
  }

  industryData(imembers, iname, itype, ipurpose) {
    try {
      const id = this.firestore.createId();
      this.firestore.doc(`industrydata/${id}`).set({
        id: id,
        members: imembers,
        industryName: iname,
        industryType: itype,
        industryPurpose: ipurpose
      });
    }
    catch (e) {
      console.log("Error: ", e);
    }
  }

}
